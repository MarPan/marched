#-------------------------------------------------
#
# Project created by QtCreator 2016-06-07T11:35:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = marched
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    duplicatesfinder.cpp

HEADERS  += mainwindow.h \
    duplicatesfinder.h

FORMS    += mainwindow.ui

win32 {
  DESTDIR = $$OUT_PWD/bin
  QMAKE_POST_LINK = $$[QT_INSTALL_BINS]/windeployqt.exe --compiler-runtime $$shell_path($$DESTDIR/$${TARGET}.exe)
}
