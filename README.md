# Marched 📂 - File Sync Checker

Marched is a simple C++/Qt application designed to help users quickly determine if files from source folders (including their subfolders) are already present in a destination folder (and its subfolders). This tool is particularly useful for managing media files and ensuring that content is safely synchronized between devices.

## Features ✨

- Easy and intuitive interface.
- Fast and accurate file comparison (files have to match by name, size, and contents to be considered "same").
- Works with subfolders, ensuring a comprehensive file comparison.
- Windows releases available in the form of convenient zip files.

## Getting Started 🚀

### Installation

1. Download the latest release of Marched from the [Releases](https://gitlab.com/MarPan/marched/-/releases) section.
2. Unpack the downloaded zip file to reveal the "marched" folder.

### Usage

1. Navigate to the "marched" folder.
2. Locate the main executable file named `marched.exe` and launch it.
3. Click "Add folder..." to select source files to search for.
4. Click the "Select folder..." button to select where the search should happen.
5. Click "Go!" 

## Example Use Case 📦

Imagine you have a USB stick filled with numerous media files, and you want to ensure that these files are safely synced to your computer before you consider wiping the USB stick. With Marched, you can effortlessly check for file presence in both locations and make informed decisions about whether it's safe to proceed.
